/* eslint-disable-next-line */
import '@babel/polyfill';
import Vue from 'vue';
import axios from 'axios';
import './plugins/vuetify';
import App from './App.vue';
import store from './store';

axios.defaults.baseURL = 'http://localhost:4000/api';

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(App),
}).$mount('#app');
