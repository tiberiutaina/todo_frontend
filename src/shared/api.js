import axios from 'axios';

export default {
  getTodos: () => axios.get('/all').then(res => res.data),
  submitTodo: obj => axios.post('/add', obj),
  completeTodo: (id, obj) => axios.post(`/update/${id}`, obj),
  deleteTodo: id => axios.get(`/delete/${id}`),
};
