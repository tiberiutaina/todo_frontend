import Vue from 'vue';
import Vuex from 'vuex';
import api from './shared/api';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    todos: [],
    newTodo: '',
  },
  getters: {
    newTodo: state => state.newTodo,
    todos: state => state.todos,
  },
  /*eslint-disable */
  mutations: {
    SET_TODOS(state, todos) {
      state.todos = todos;
    },
    ADD_TODO(state, todoObject) {
      state.todos.push(todoObject);
    },
    CLEAR_NEW_TODO(state) {
      state.newTodo = '';
    },
  },
  actions: {
    loadTodos({ commit }) {
      api.getTodos().then((todos) => {
        commit('SET_TODOS', todos);
      });
    },
    completeTodo({ commit }, todo) {
      const payload = {
        name: todo.name,
        completed: todo.completed,
      };

      api.completeTodo(todo._id, payload).then(() => {
        api.getTodos().then((todos) => {
          commit('SET_TODOS', todos);
        });
      });
    },
    deleteTodo({ commit }, todo) {
      api.deleteTodo(todo._id).then(() => {
        api.getTodos().then((todos) => {
          commit('SET_TODOS', todos);
        });
      });
    },
    addTodo({ commit }, todoName) {
      const todo = {
        name: todoName,
        completed: false,
      };

      api.submitTodo(todo).then(() => {
        api.getTodos().then((todos) => {
          commit('SET_TODOS', todos);
        });
      });
    },
  },
});
